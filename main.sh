#!/bin/bash

#args 1=
#Filename: Title-Artist-Album.xxx
#echo "first arg: "$1

#declare -a localDirectoryPath
#echo "${BASH_SOURCE[*]}"
#echo "len "${BASH_SOURCE[*]}

filepathString="${BASH_SOURCE[*]}"
filepathArray=($(echo $filepathString | tr "/" "\n"))

if [[ "$(declare -p filepathArray)" =~ "declare -a" ]]; then
    echo 'array'
else
    echo 'no array'
fi

#Print the split string
for((i in "${filepathArray[@]}"))do
  if i < ${#filepathArray[@]}
    echo $i
  else
    echo 'last'
  fi
done

for filename in /Users/arvidmarklund/Documents/_MEDIA/MUSIK_OSORTERAD/_INVIDIOUS/*.*; do
    SAVEIFS=$IFS   # Save current IFS
    IFS=$'/'      # Change IFS to new line

    filenames=($filename) # split to array $names
    metadata="${filenames[${#filenames[@]}-1]}"
    IFS=$'-'
    #echo $metadata;
    metadata=($metadata) # split to array
    echo "";
    echo "${metadata[0]}"
    echo "${metadata[1]}"
    echo "${metadata[2]}"
    IFS=$'.'
    albumarray=(${metadata[2]})
    echo "${albumarray[0]}"

    echo "$filename"
    ffmpeg -i "$filename" -acodec mp3 -b:a 320k -metadata title="${metadata[0]}" -metadata artist="${metadata[1]}" -metadata album="${albumarray[0]}" "${metadata[0]}${metadata[1]}${albumarray[0]}".mp3
    IFS=$SAVEIFS   # Restore IFS
  done
